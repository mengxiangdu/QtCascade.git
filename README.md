# QtCascade

#### 介绍
实现Cascade纠错协议的Demo。常用领域是量子密钥分发（QKD）系统

#### 软件架构
Alice是一个线程，Bob是另一个线程。本程序使用一个unsigned int代表一个数据位。线程之间通过Qt信号槽传递数据。Cascade的主要逻辑就是反复用二分法对比Alice和Bob的奇偶校验码查找错误比特并纠正

#### 安装教程

1.  不用安装，编译之后直接运行

#### 使用说明

1.  生成密钥
2.  纠错

#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
