#include "CreateKeys.h"

CreateKeys::CreateKeys(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
}

CreateKeys::~CreateKeys()
{
}

int CreateKeys::keyCount() const
{
	return ikeyCount;
}

float CreateKeys::errorRatio() const
{
	return ierrorRatio;
}

void CreateKeys::on_pbCancel_clicked()
{
	reject();
}

void CreateKeys::on_pbOk_clicked()
{
	ikeyCount = ui.sbKeyCount->value();
	ierrorRatio = ui.dsbErrorRatio->value();
	accept();
}



