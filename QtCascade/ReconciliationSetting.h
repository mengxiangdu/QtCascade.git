#pragma once

#include <QDialog>
#include "ui_ReconciliationSetting.h"

class ReconciliationSetting : public QDialog
{
	Q_OBJECT

public:
	ReconciliationSetting(QWidget *parent = Q_NULLPTR);
	~ReconciliationSetting();
	int exec();

private slots:
	void on_pbOk_clicked();
	void on_pbCancel_clicked();

private:
	Ui::ReconciliationSetting ui;
};



