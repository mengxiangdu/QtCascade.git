#pragma once

#include <QtWidgets/QMainWindow>
#include "ui_QtCascade.h"
#include "Cascade.h"

class QtCascade : public QMainWindow
{
    Q_OBJECT

public:
    QtCascade(QWidget *parent = Q_NULLPTR);

private:
	QString arrayToString(const QVector<uint>& array);
	int errorKeyCount(const QVector<uint>& a, const QVector<uint>& b, QString& htmla, QString &htmlb);
	void createNewKeys(int count, float ratio);

private slots:
	void on_pteAKeys_selectionChanged();
	void on_pteBKeys_selectionChanged();
	void on_pteAKeys_cursorPositionChanged();
	void on_pteBKeys_cursorPositionChanged();
	void on_actGuide_triggered();
	void on_actExit_triggered();
	void on_actCreateKey_triggered();
	void on_actCorrect_triggered();
	void correctFinished();
	void pteaValueChanged(int value);
	void ptebValueChanged(int value);

private:
    Ui::QtCascadeClass ui;
	QLabel* lbCursorPos;
	MAlice alice;
	MBob bob;
	float errorRatioSaved;
};



