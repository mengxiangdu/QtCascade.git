#include "QtCascade.h"
#include "CreateKeys.h"
#include "ReconciliationSetting.h"
#include "qscrollbar.h"
#include "qmessagebox.h"
#include "qdebug.h"
#include <random>

using std::random_device;
using std::mt19937;
using std::uniform_int_distribution;

QtCascade::QtCascade(QWidget *parent)
    : QMainWindow(parent)
{
    ui.setupUi(this);

	lbCursorPos = new QLabel(this);
	lbCursorPos->setMinimumWidth(80);
	statusBar()->addPermanentWidget(lbCursorPos);

	QScrollBar* vbar1 = ui.pteAKeys->verticalScrollBar();
	connect(vbar1, &QScrollBar::valueChanged, this, &QtCascade::pteaValueChanged);
	QScrollBar* vbar2 = ui.pteBKeys->verticalScrollBar();
	connect(vbar2, &QScrollBar::valueChanged, this, &QtCascade::ptebValueChanged);

	connect(&alice, &MAlice::sendGroup, &bob, &MBob::receiveGroup);
	connect(&alice, &MAlice::sendPairty, &bob, &MBob::receiveParity);
	connect(&bob, &MBob::sendBinarySegment, &alice, &MAlice::receiveBinarySegment);
	connect(&bob, &MBob::sendBeginCorrect, &alice, &MAlice::beginCorrect);
	connect(&bob, &MBob::finished, this, &QtCascade::correctFinished);
	qRegisterMetaType<Group>("Group");
	qRegisterMetaType<QVector<Group>>("QVector<Group>");
}

void QtCascade::pteaValueChanged(int value)
{
	QScrollBar* vbar = ui.pteBKeys->verticalScrollBar();
	if (vbar->value() != value)
	{
		vbar->setValue(value);
	}
}

void QtCascade::ptebValueChanged(int value)
{
	QScrollBar* vbar = ui.pteAKeys->verticalScrollBar();
	if (vbar->value() != value)
	{
		vbar->setValue(value);
	}
}

void QtCascade::on_pteAKeys_selectionChanged()
{
	QTextCursor cursora = ui.pteAKeys->textCursor();
	int sa = cursora.selectionStart();
	int ta = cursora.selectionEnd();
	QTextCursor cursorb = ui.pteBKeys->textCursor();
	int sb = cursorb.selectionStart();
	int tb = cursorb.selectionEnd();
	if (sa != sb || ta != tb)
	{
		cursorb.setPosition(sa);
		cursorb.movePosition(QTextCursor::Right, QTextCursor::KeepAnchor, ta - sa);
		ui.pteBKeys->setTextCursor(cursorb);
	}
}

void QtCascade::on_pteBKeys_selectionChanged()
{
	QTextCursor cursora = ui.pteAKeys->textCursor();
	int sa = cursora.selectionStart();
	int ta = cursora.selectionEnd();
	QTextCursor cursorb = ui.pteBKeys->textCursor();
	int sb = cursorb.selectionStart();
	int tb = cursorb.selectionEnd();
	if (sa != sb || ta != tb)
	{
		cursora.setPosition(sb);
		cursora.movePosition(QTextCursor::Right, QTextCursor::KeepAnchor, tb - sb);
		ui.pteAKeys->setTextCursor(cursorb);
	}
}

void QtCascade::on_pteAKeys_cursorPositionChanged()
{
	QTextCursor cursora = ui.pteAKeys->textCursor();
	int pos = cursora.position();
	lbCursorPos->setText(u8"光标位置    " + QString::number(pos));
}

void QtCascade::on_pteBKeys_cursorPositionChanged()
{
	QTextCursor cursorb = ui.pteBKeys->textCursor();
	int pos = cursorb.position();
	lbCursorPos->setText(u8"光标位置    " + QString::number(pos));
}

void QtCascade::	on_actGuide_triggered()
{
	QMessageBox::information(this, u8"使用说明",
		u8"使用方法如下：\n"
		u8"1、点击“生成密钥”按钮按照设置产生密钥；\n"
		u8"2、点击“纠错”按钮以指定轮数纠错。\n"
		u8"PS：此方法不能保证100%纠错");
}

void QtCascade::on_actExit_triggered()
{
	close();
}

void QtCascade::on_actCreateKey_triggered()
{
	CreateKeys dialog;
	if (QDialog::Rejected == dialog.exec())
	{
		/* 取消生成密钥 */
		return;
	}
	int count = dialog.keyCount();
	float ratio = dialog.errorRatio();
	createNewKeys(count, ratio);
	errorRatioSaved = ratio; /* 保存误码率纠错用 */

	const QVector<uint>& akeys = alice.keys();
	const QVector<uint>& bkeys = bob.keys();
	QString texta, textb;
	int ecount = errorKeyCount(akeys, bkeys, texta, textb);
	ui.pteAKeys->clear();
	ui.pteBKeys->clear();
	ui.pteAKeys->appendHtml(texta);
	ui.pteBKeys->appendHtml(textb);

	QTextCursor cursora = ui.pteAKeys->textCursor();
	cursora.movePosition(QTextCursor::Start);
	ui.pteAKeys->setTextCursor(cursora);
	QTextCursor cursorb = ui.pteBKeys->textCursor();
	cursorb.movePosition(QTextCursor::Start);
	ui.pteBKeys->setTextCursor(cursorb);

	QString text = QString(u8"已生成密钥，共有%1处不同").arg(ecount);
	statusBar()->showMessage(text, 5000);
}

void QtCascade::createNewKeys(int count, float ratio)
{
	int thres = (1 - ratio) * 100;
	QVector<uint> akeys;
	QVector<uint> bkeys;
	mt19937 mt(random_device{}());
	uniform_int_distribution<int> rand(0, 1);
	uniform_int_distribution<int> rate(1, 100);
	for (int i = 0; i < count; i++)
	{
		uint x = rand(mt);
		int equal = rate(mt) <= thres; /* 相同的概率[0,100] */
		if (equal)
		{
			akeys.append(x);
			bkeys.append(x);
		}
		else
		{
			akeys.append(x);
			bkeys.append(1 - x);
		}
	}
	alice.setKeys(akeys);
	bob.setKeys(bkeys);
}

void QtCascade::on_actCorrect_triggered()
{
	ReconciliationSetting dialog;
	int loop = dialog.exec();
	if (loop > 0)
	{
		alice.correct(loop, errorRatioSaved);
	}
}

void QtCascade::correctFinished()
{
	const QVector<uint>& bkeys = bob.keys();
	const QVector<uint>& akeys = alice.keys();
	QString texta, textb;
	int ecount = errorKeyCount(akeys, bkeys, texta, textb);
	ui.pteAKeys->clear();
	ui.pteBKeys->clear();
	ui.pteAKeys->appendHtml(texta);
	ui.pteBKeys->appendHtml(textb);

	QTextCursor cursora = ui.pteAKeys->textCursor();
	cursora.movePosition(QTextCursor::Start);
	ui.pteAKeys->setTextCursor(cursora);
	QTextCursor cursorb = ui.pteBKeys->textCursor();
	cursorb.movePosition(QTextCursor::Start);
	ui.pteBKeys->setTextCursor(cursorb);

	QString text = QString(u8"纠错后仍有%1处不同").arg(ecount);
	statusBar()->showMessage(text, 5000);
}

QString QtCascade::arrayToString(const QVector<uint>& array)
{
	QString text;
	for (uint item : array)
	{
		text.append(QString::number(item));
	}
	return text;
}


int QtCascade::errorKeyCount(const QVector<uint>& a, const QVector<uint>& b, QString& htmla, QString &htmlb)
{
	int ecount = 0;
	int count = a.size();
	for (int i = 0; i < count; i++)
	{
		if (a[i] != b[i])
		{
			htmla += QString(u8"%1").arg(a[i]);
			htmlb += QString(u8R"(<span style="background-color: #ef7c7c;">%1</span>)").arg(b[i]);
			ecount++;
		}
		else
		{
			htmla += QString(u8"%1").arg(a[i]);
			htmlb += QString(u8"%1").arg(b[i]);
		}
	}
	return ecount;
}






