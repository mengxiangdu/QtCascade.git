#include "ReconciliationSetting.h"

ReconciliationSetting::ReconciliationSetting(QWidget *parent)
	: QDialog(parent)
{
	ui.setupUi(this);
}

ReconciliationSetting::~ReconciliationSetting()
{
}

int ReconciliationSetting::exec()
{
	int result = QDialog::exec();
	if (QDialog::Accepted == result)
	{
		return ui.sbLoopCount->value();
	}
	return 0;
}

void ReconciliationSetting::on_pbOk_clicked()
{
	accept();
}

void ReconciliationSetting::on_pbCancel_clicked()
{
	reject();
}




