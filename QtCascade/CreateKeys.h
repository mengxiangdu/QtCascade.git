#pragma once

#include <QDialog>
#include "ui_CreateKeys.h"

class CreateKeys : public QDialog
{
	Q_OBJECT

public:
	CreateKeys(QWidget *parent = Q_NULLPTR);
	~CreateKeys();
	int keyCount() const;
	float errorRatio() const;

private slots:
	void on_pbCancel_clicked();
	void on_pbOk_clicked();

private:
	Ui::CreateKeys ui;
	int ikeyCount;
	float ierrorRatio;
};



