/////////////////////////////////////////////////////////////////////////////////////////
// Cascade协议实现文档。Alice是电脑一；Bob是电脑二
//
// 孟祥度，@2024年1月12日
// 创建初始文档
/////////////////////////////////////////////////////////////////////////////////////////
#include "Cascade.h"
#include "qapplication.h"
#include "qdebug.h"
#include <set>
#include <random>

using std::set;
using std::random_device;
using std::mt19937;
using std::uniform_int_distribution;

MAlice::MAlice(QObject* parent) :
	QObject(parent)
{
	connect(this, &MAlice::asyncBeginCorrect, this, &MAlice::beginCorrect);
	moveToThread(&workThr);
	workThr.start();
}

MAlice::~MAlice()
{
	workThr.quit();
	workThr.wait();
}

void MAlice::setKeys(const QVector<uint>& ikeys)
{
	myKeys = ikeys;
}

const QVector<uint>& MAlice::keys() const
{
	return myKeys;
}

//---------------------------------------------------------------------------------------
// 以指定的轮数和估计的误码率纠错
// loopCount：轮数
// errorRatio：估计的误码率
//---------------------------------------------------------------------------------------
void MAlice::correct(int loopCount, float errorRatio)
{
	sep = 1 / errorRatio;
	emit asyncBeginCorrect(loopCount, 0);
}

void MAlice::beginCorrect(int loopCount, int thisLoop)
{
	int count = myKeys.size();
	/* 此处计算随机区间的最小最大长度 */
	int smin = (1 + thisLoop * 0.1f) * 0.73f * sep;
	int smax = smin * 2;
	smin = std::max(smin, 2);
	smax = std::min(smax, count / 4);

	random_device seed;
	mt19937 mt(seed());
	uniform_int_distribution<int> dist(smin, smax);
	QVector<Group> segments;
	int left = count;
	while (left > smax)
	{
		int value = dist(mt);
		Group temp;
		if (segments.empty())
		{
			temp.start = 0;
			temp.end = value;
		}
		else
		{
			temp.start = segments.back().end;
			temp.end = temp.start + value;
		}
		segments.push_back(temp);
		left -= value;
	}
	Group last;
	last.start = segments.back().end;
	last.end = last.start + left;
	segments.push_back(last);

	for (Group& item : segments)
	{
		item.parity = calcParity(item.start, item.end);
	}
	emit sendGroup(segments, loopCount, thisLoop);
}

void MAlice::receiveBinarySegment(int start, int end, bool *completed)
{
	int parity = calcParity(start, end);
	emit sendPairty(parity, completed);
}

int MAlice::calcParity(int start, int end)
{
	int bcount = 0;
	for (int i = start; i < end; i++)
	{
		bcount += myKeys[i];
	}
	bcount &= 1;
	return bcount;
}

/////////////////////////////////////////////////////////////////////////////////////////

MBob::MBob(QObject* parent) :
	QObject(parent)
{
	moveToThread(&workThr);
	workThr.start();
}

MBob::~MBob()
{
	workThr.quit();
	workThr.wait();
}

void MBob::setKeys(const QVector<uint>& ikeys)
{
	myKeys = ikeys;
}

const QVector<uint>& MBob::keys() const
{
	return myKeys;
}

void MBob::receiveGroup(const QVector<Group>& groups, int loopCount, int thisLoop)
{
	execCorrectOnce(groups);
	thisLoop += 1;
	if (thisLoop < loopCount)
	{
		emit sendBeginCorrect(loopCount, thisLoop);
	}
	else
	{
		emit finished();
		history.clear();
	}
}

void MBob::execCorrectOnce(const QVector<Group>& groups)
{
    history.push_back(QVector<Group>());
	for (const auto& item : groups)
	{
		int parity = calcParity(item.start, item.end);
        while (parity != item.parity)
        {
            /* 二分法纠错 */
            int errorKey = execBinaryCorrect(item);
            flipKey(errorKey); /* 反转错误的位 */
            QVector<Group> M;
            findFromHistory(errorKey, item, M);
            while (!M.empty())
            {
                auto minv = std::min_element(M.begin(), M.end(),
                    [](Group a, Group b) { return a.end - a.start < b.end - b.start; });
                int errorIndex = execBinaryCorrect(*minv);
                flipKey(errorIndex);
                QVector<Group> Q;
                findFromHistory(errorIndex, item, Q);
                M = xor(M, Q);
            }
            parity = calcParity(item.start, item.end);
        }
        history.back().push_back(item);
	}
}

int MBob::execBinaryCorrect(const Group& segment)
{
	binary.start = segment.start;
	binary.end = segment.end;
	while (binary.end - binary.start > 1)
	{
		bool completed = false;
		binary.cstart = binary.start;
		binary.cend = (binary.start + binary.end) / 2;
		emit sendBinarySegment(binary.cstart, binary.cend, &completed);
		while (!completed)
		{
			QApplication::processEvents();
		}
	}
	return binary.start;
}

QVector<Group> MBob::xor(const QVector<Group>& a, const QVector<Group>& b)
{
	QVector<Group> sum;
	int acount = a.size();
	for (int i = 0; i < acount; i++)
	{
		if (!b.contains(a[i]))
		{
			sum.append(a[i]);
		}
	}
	int bcount = b.size();
	for (int i = 0; i < bcount; i++)
	{
		if (!a.contains(b[i]))
		{
			sum.append(b[i]);
		}
	}
	return sum;
}

void MBob::receiveParity(int value, bool *completed)
{
	int parity = calcParity(binary.cstart, binary.cend);
	if (value == parity) /* 后半段有问题 */
	{
		binary.start = binary.cend;
	}
	else /* 前半段有问题 */
	{
		binary.end = binary.cend;
	}
	*completed = true;
}

void MBob::findFromHistory(int keyIndex, const Group& exclude, QVector<Group> &past)
{
	for (const auto& each : history)
	{
		auto found = std::find_if(each.begin(), each.end(),
			[keyIndex](Group x) { return keyIndex >= x.start && keyIndex < x.end; });
		if (found != each.end() && *found != exclude)
		{
			past.push_back(*found);
		}
	}
}

int MBob::calcParity(int start, int end)
{
	int bcount = 0;
	for (int i = start; i < end; i++)
	{
		bcount += myKeys[i];
	}
	bcount &= 1;
	return bcount;
}

void MBob::flipKey(int index)
{
	myKeys[index] = 1 - myKeys[index];
}




